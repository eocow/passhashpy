import hashlib, sys, codecs

# creates the global var of logintof which is a ToF
# used later
loginqna = input("Are you logging in or registering?: ")
if loginqna == "logging in":
  logintof = True
if loginqna == "login":
  logintof = True
elif loginqna == "registering":
  logintof = False 
elif loginqna == "reg":
  logintof = False
else:
  print("Invalid Response")
  sys.exit()

usrinp = input("Enter your Password: ")
usrinpd = usrinp.encode("utf-8")
usrhash = hashlib.sha256()
usrhash.update(usrinpd)
temphash = usrhash.hexdigest()

# for debug
#print(temphash)
#print(logintof)

# here logintof is used to direct either to find the password hash in the txt 
if logintof == True:
  passwrd = open("./passwrd.txt", "r")
  flag = 0
  index = 0
  for line in passwrd:
    index += 1 
    if temphash in line:
      flag = 1
      break 
  if flag == 1: 
    print("Correct Password!")
    passwrd.close
    sys.exit()
  else:
    print("Incorrect Password.")
    passwrd.close()
    sys.exit()
elif logintof == False:
  passwrda = open("./passwrd.txt", "a")
  passwrda.write("\n" + temphash)
  passwrda.close()
  print("You are now registered!")
  sys.exit()
